package ru.t1.sochilenkov.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.sochilenkov.tm.dto.model.ProjectDTO;

@Repository
public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

}
