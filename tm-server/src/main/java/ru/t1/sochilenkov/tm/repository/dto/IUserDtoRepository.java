package ru.t1.sochilenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.sochilenkov.tm.dto.model.UserDTO;

@Repository
public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    boolean existsByLogin(@NotNull String login);

    boolean existsByEmail(@NotNull String email);

}
