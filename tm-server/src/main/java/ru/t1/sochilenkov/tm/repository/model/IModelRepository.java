package ru.t1.sochilenkov.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.sochilenkov.tm.model.AbstractModel;

@NoRepositoryBean
public interface IModelRepository<M extends AbstractModel> extends JpaRepository<M, String> {
}
